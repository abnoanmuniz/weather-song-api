﻿namespace WeatherSong.Utils
{
    public class WsSettings
    {
        public string ConnectionString { get; set; }
        public string Secret { get; set; }
    }
}
