﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace WeatherSong.Utils
{
    public static class Md5Util
    {
        public static string ConvertStringToHash(string text)
        {
            using var md5Hash = MD5.Create();
            var sourceBytes = Encoding.UTF8.GetBytes(text);
            var hashBytes = md5Hash.ComputeHash(sourceBytes);
            var hash = BitConverter.ToString(hashBytes).ToLower().Replace("-", string.Empty);
            return hash;
        }

        public static string ConvertHashToString(string hash)
        {
            string hashConverted = BitConverter.ToString(Guid.Parse(hash).ToByteArray()).ToUpper().Replace("-", string.Empty);
            return hashConverted;
        }
    }
}
