﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace WeatherSong.Repository.EntitiesConfiguration
{
    public abstract class BaseEntityConfiguration<T> : IEntityTypeConfiguration<T> where T : class
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.Property<int>("Id")
                 .HasColumnName("Id")
                 .ValueGeneratedOnAdd();

            builder.Property<DateTime>("CreatedDate")
                .HasColumnName("Created_date")
                .IsRequired(true)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("getdate()");

            builder.Property<DateTime?>("ModifiedDate")
               .HasColumnName("Modified_date");
        }
    }
}
