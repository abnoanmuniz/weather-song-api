﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WeatherSong.Models.Entities;

namespace WeatherSong.Repository.EntitiesConfiguration
{
    public class PersonalNoteConfiguration : BaseEntityConfiguration<PersonalNote>
    {
        public override void Configure(EntityTypeBuilder<PersonalNote> builder)
        {
            builder.Ignore("Id");
            builder.Ignore("Created_date");
            builder.Ignore("Modified_date");

            builder.Property(c => c.Note).HasColumnName("Note");
            builder.Property(c => c.UserId).HasColumnName("LoggedUser_id");

            base.Configure(builder);
        }
    }
}
