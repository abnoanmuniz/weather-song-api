﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WeatherSong.Models.Entities;

namespace WeatherSong.Repository.EntitiesConfiguration
{
    public class UserConfiguration : BaseEntityConfiguration<User>
    {
        public override void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Ignore("Id");
            builder.Ignore("CreatedDate");
            builder.Ignore("ModifiedDate");

            builder.Property(c => c.Email).HasColumnName("Email").HasMaxLength(255);
            builder.Property(c => c.Hometown).HasColumnName("Hometown").HasMaxLength(255);
            builder.Property(c => c.Name).HasColumnName("Name").HasMaxLength(255);
            builder.Property(c => c.Password).HasColumnName("Password").HasMaxLength(255);

            builder.HasMany(r => r.PersonalNotes)
                   .WithOne(c => c.User)
                   .HasForeignKey(s => s.UserId)
                   .OnDelete(DeleteBehavior.Cascade);

            base.Configure(builder);
        }
    }
}
