﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WeatherSong.Models.Entities;
using WeatherSong.Utils.Exceptions;

namespace WeatherSong.Repository.SQL
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        public readonly WsDbContext _context;
        public BaseRepository(WsDbContext context)
        {
            _context = context;
        }
        public async Task Create(TEntity entity)
        {
            entity.CreatedDate = DateTime.Now;
            await _context.Set<TEntity>().AddAsync(entity);
            await _context.SaveChangesAsync();
        }
        public async Task Delete(int id)
        {
            var entity = await GetById(id);
            _context.Set<TEntity>().Remove(entity);
            await _context.SaveChangesAsync();
        }
        public IQueryable<TEntity> GetAll()
        {
            return _context.Set<TEntity>().AsNoTracking();
        }
        public async Task<TEntity> GetByIdAsNoTracking(int id)
        {
            var entity = await _context.Set<TEntity>()
               .AsNoTracking()
               .FirstOrDefaultAsync(e => e.Id == id);
            if (entity is null)
            {
                throw new EntityNotFoundException(typeof(TEntity).FullName);
            }
            return entity;
        }
        public async Task<TEntity> GetById(int id)
        {
            var ret = await _context.Set<TEntity>()
                .FirstOrDefaultAsync(e => e.Id == id);
            if (ret == null)
            {
                throw new EntityNotFoundException(typeof(TEntity).FullName);
            }
            return ret;
        }
        public async Task Update(TEntity entity)
        {
            entity.ModifiedDate = DateTime.Now;
            _context.Set<TEntity>().Update(entity);
            await _context.SaveChangesAsync();
        }
        public async Task AddCollectionAsync(IEnumerable<TEntity> entities)
        {
            await _context.AddRangeAsync(entities).ConfigureAwait(false);
            await _context.SaveChangesAsync();
        }
        public Task UpdateCollectionAsync(IEnumerable<TEntity> entities)
        {
            _context.UpdateRange(entities);
            return Task.CompletedTask;
        }
        public ICollection<TEntity> FindAll(Expression<Func<TEntity, bool>> match)
        {
            return _context.Set<TEntity>().Where(match).ToList();
        }
        public async Task<ICollection<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> match)
        {
            return await _context.Set<TEntity>().Where(match).ToListAsync();
        }
        public async Task<TEntity> FindAsyncAsNoTracking(Expression<Func<TEntity, bool>> match)
        {
            return await _context.Set<TEntity>().AsNoTracking().SingleOrDefaultAsync(match);
        }
        public async Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> match)
        {
            return await _context.Set<TEntity>().SingleOrDefaultAsync(match);
        }
        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
