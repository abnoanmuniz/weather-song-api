﻿using WeatherSong.Models.Entities;

namespace WeatherSong.Repository.SQL.Users
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private readonly WsDbContext context;
        public UserRepository(WsDbContext context) : base(context)
        {
            this.context = context;
        }
    }
}
