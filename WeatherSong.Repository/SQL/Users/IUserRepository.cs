﻿using WeatherSong.Models.Entities;

namespace WeatherSong.Repository.SQL.Users
{
    public interface IUserRepository : IBaseRepository<User>
    {
    }
}
