﻿using WeatherSong.Models.Entities;

namespace WeatherSong.Repository.SQL.PersonalNotes
{

    public class PersonalNoteRepository : BaseRepository<PersonalNote>, IPersonalNoteRepository
    {
        private readonly WsDbContext context;
        public PersonalNoteRepository(WsDbContext context) : base(context)
        {
            this.context = context;
        }
    }
}

