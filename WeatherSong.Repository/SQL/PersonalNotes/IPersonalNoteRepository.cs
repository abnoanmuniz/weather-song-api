﻿using WeatherSong.Models.Entities;

namespace WeatherSong.Repository.SQL.PersonalNotes
{
    public interface IPersonalNoteRepository : IBaseRepository<PersonalNote>
    {
    }
}
