﻿using Microsoft.EntityFrameworkCore;
using WeatherSong.Models.Entities;
using WeatherSong.Repository.EntitiesConfiguration;

namespace WeatherSong.Repository
{
    public partial class WsDbContext : DbContext
    {
        public WsDbContext(DbContextOptions<WsDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("LoggedUser")
                .HasKey(c => new { c.Id });
            modelBuilder.ApplyConfiguration(new UserConfiguration());

            modelBuilder.Entity<PersonalNote>().ToTable("Personal_note")
                .HasKey(c => new { c.Id });
            modelBuilder.ApplyConfiguration(new PersonalNoteConfiguration());

        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLazyLoadingProxies(true);
            }
        }

        #region DbSet
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<PersonalNote> PersonalNotes { get; set; }
        #endregion
    }
}
