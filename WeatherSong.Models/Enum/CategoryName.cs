﻿namespace WeatherSong.Models.Enum
{
    public enum CategoryName
    {
        Party,
        Pop,
        Rock,
        Classical
    }
}