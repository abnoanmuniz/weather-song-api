﻿namespace WeatherSong.Models.Entities
{
    public class PersonalNote : BaseEntity
    {
        public PersonalNote()
        {         
          
        }
        public virtual User User { get; set; }
        public int UserId { get; set; }
        public string Note { get; set; }
    }
}
