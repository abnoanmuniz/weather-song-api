﻿using System.Collections.Generic;


namespace WeatherSong.Models.Entities
{
    public class User : BaseEntity
    {
        public User()
        {
            PersonalNotes = new List<PersonalNote>();
        }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Hometown { get; set; }
        public string Password { get; set; }        
        public virtual IList<PersonalNote> PersonalNotes { get; set; }
    }
}
