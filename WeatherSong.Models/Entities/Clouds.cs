﻿using Newtonsoft.Json;

namespace WeatherSong.Models.Entities
{
    public class Clouds
    {
        [JsonProperty("all")]
        public int All { get; set; }
    }
}
