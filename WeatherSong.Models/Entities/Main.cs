﻿using Newtonsoft.Json;
using WeatherSong.Utils.ExtensionMethods;

namespace WeatherSong.Models.Entities
{
    public class Main
    {
        [JsonProperty("temp")]
        public double Temp { get; set; }

        [JsonProperty("pressure")]
        public int Pressure { get; set; }

        [JsonProperty("humidity")]
        public int Humidity { get; set; }

        [JsonProperty("temp_min")]
        public double TempMin { get; set; }

        [JsonProperty("temp_max")]
        public double TempMax { get; set; }

        [JsonIgnore]
        public double TempCelsius
        {
            get
            {
                return Temp.KelvinToCelsius();
            }
        }

        [JsonIgnore]
        public double TempMaxCelsius
        {
            get
            {
                return TempMax.KelvinToCelsius();
            }
        }

        [JsonIgnore]
        public double TempMinCelsius
        {
            get
            {
                return TempMin.KelvinToCelsius();
            }
        }
    }
}
