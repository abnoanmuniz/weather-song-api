﻿using Newtonsoft.Json;

namespace WeatherSong.Models.Entities
{
    public class Weather
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("main")]
        public string Main { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("con")]
        public string Icon { get; set; }
    }
}
