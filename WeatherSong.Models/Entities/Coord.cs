﻿using Newtonsoft.Json;

namespace WeatherSong.Models.Entities
{
    public class Coord
    {
        [JsonProperty("lon")]
        public double Lon { get; set; }

        [JsonProperty("lat")]
        public double Lat { get; set; }
    }
}
