﻿using System;

namespace WeatherSong.Models.DTOs
{
    public abstract class BaseEntityDTO
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
