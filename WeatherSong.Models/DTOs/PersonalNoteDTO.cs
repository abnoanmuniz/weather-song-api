﻿namespace WeatherSong.Models.DTOs
{
    public class PersonalNoteDTO : BaseEntityDTO
    {
        public virtual UserDTO User { get; set; }
        public int UserId { get; set; }
        public string Note { get; set; }
    }
}
