﻿using System.Collections.Generic;

namespace WeatherSong.Models.DTOs
{
    public class UserDTO : BaseEntityDTO
    {
        public UserDTO()
        {
            PersonalNotes = new List<PersonalNoteDTO>();
        }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Hometown { get; set; }
        public string Password { get; set; }
        public virtual IList<PersonalNoteDTO> PersonalNotes { get; set; }
    }
}
