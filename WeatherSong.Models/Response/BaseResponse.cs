﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using WeatherSong.Utils.ExtensionMethods;

namespace WeatherSong.Models.Response
{
    public class BaseResponse<TEntity>
    {
        public BaseResponse()
        {
            Exception = new ExceptionResponse();         
        }
        public HttpStatusCode httpStatusCode { get; set; }  
        public string Message { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TEntity Data { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<TEntity> Entities { get; set; }

        public ExceptionResponse Exception { get; set; }
        public BaseResponse<TEntity> CreateResponse(Exception exception)
        {
            this.Exception.StackTrace = exception.StackTrace;
            this.Exception.InnerExceptions = exception.GetInnerExceptions();          
            return this;
        }   
    }
}
