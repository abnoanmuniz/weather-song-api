﻿using WeatherSong.Models.Entities;

namespace WeatherSong.Models.Response
{
    public class AuthenticateResponse
    {
        public int Id { get; set; } 
        public string Email { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }

        public AuthenticateResponse(User user, string token)
        {
            Id = user.Id;            
            Email = user.Email;
            Name = user.Name;
            Token = token;
        }
    }
}
