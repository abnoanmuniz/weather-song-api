﻿namespace WeatherSong.Models.Response
{
    public class ExceptionResponse
    {
        public string InnerExceptions { get; set; }
        public string StackTrace { get; set; }
    }
}
