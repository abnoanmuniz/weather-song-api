﻿using AutoMapper;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WeatherSong.Models;
using WeatherSong.Models.DTOs;
using WeatherSong.Models.Entities;
using WeatherSong.Models.Response;
using WeatherSong.Repository.SQL.Users;
using WeatherSong.Utils;

namespace WeatherSong.Services.Users
{
    public class UserService : IUserService
    {
        private readonly IMapper mapper;
        private readonly IUserRepository userRepository;
        private readonly WsSettings wsSettings;
        public UserService(IUserRepository userRepository, IMapper mapper, IOptions<WsSettings> wsSettings)
        {
            this.userRepository = userRepository;
            this.mapper = mapper;
            this.wsSettings = wsSettings.Value;
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var user = userRepository.FindAll(s => s.Name == model.Username && s.Password == Md5Util.ConvertStringToHash(model.Password)).SingleOrDefault();

            // return null if user not found
            if (user == null) return null;

            // authentication successful so generate jwt token
            var token = generateJwtToken(user);

            return new AuthenticateResponse(user, token);
        }

        public async Task<BaseResponse<object>> Create(UserDTO user)
        {
            FillPersonalNote(user);
            var model = mapper.Map<User>(user);
            await userRepository.Create(model);
            var response = new BaseResponse<object>
            {
                Data = mapper.Map<UserDTO>(model)
            };
            return response;
        }

        public async Task<BaseResponse<object>> GetById(int userId)
        {
            var model = await userRepository.GetById(userId);
            var response = new BaseResponse<object>
            {
                Data = model
            };
            return response;
        }

        public async Task<BaseResponse<object>> Update(UserDTO user)
        {
            var entity = await userRepository.GetById(user.Id);
            var entityToUpdate = mapper.Map(user, entity);
            await userRepository.Update(entityToUpdate);

            var response = new BaseResponse<object>
            {
                Data = mapper.Map<UserDTO>(entity)
            };
            return response;
        }

        public async Task<BaseResponse<object>> Delete(int userId)
        {
            await userRepository.GetByIdAsNoTracking(userId);
            await userRepository.Delete(userId);

            var response = new BaseResponse<object> { };
            return response;
        }

        public async Task<BaseResponse<object>> RemindPassword(int userId)
        {
            var user = await userRepository.GetById(userId);
            var password = Md5Util.ConvertHashToString(user.Password);

            var response = new BaseResponse<object>
            {
                Data = password
            };
            return response;
        }

        public async Task<BaseResponse<object>> ResetPassword(int userId, string newPassword)
        {
            var user = await userRepository.GetById(userId);
            user.Password = Md5Util.ConvertStringToHash(newPassword);
            await userRepository.Update(user);

            var response = new BaseResponse<object>
            {
                Data = user
            };
            return response;
        }


        #region private methods
        private void FillPersonalNote(UserDTO user)
        {
            if (user.PersonalNotes != null)
            {
                foreach (var item in user.PersonalNotes)
                {
                    item.CreatedDate = DateTime.Now;
                    item.UserId = user.Id;
                }
            }
        }

        private string generateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(wsSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        #endregion
    }
}


