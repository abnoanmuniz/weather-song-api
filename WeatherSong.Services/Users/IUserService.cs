﻿using System.Threading.Tasks;
using WeatherSong.Models;
using WeatherSong.Models.DTOs;
using WeatherSong.Models.Response;

namespace WeatherSong.Services.Users
{
    public interface IUserService
    {
        Task<BaseResponse<object>> GetById(int userId);
        Task<BaseResponse<object>> Create(UserDTO user);
        Task<BaseResponse<object>> Update(UserDTO user);
        Task<BaseResponse<object>> Delete(int userId);
        Task<BaseResponse<object>> RemindPassword(int userId);
        Task<BaseResponse<object>> ResetPassword(int userId, string newPassword);
        AuthenticateResponse Authenticate(AuthenticateRequest model);
    }
}
