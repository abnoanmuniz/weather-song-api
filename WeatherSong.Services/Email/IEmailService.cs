﻿using System.Threading.Tasks;
using WeatherSong.Models.Entities;

namespace WeatherSong.Services.Email
{
    public interface IEmailService
    {
        Task RemindPassword(User user, string password);
    }
}
