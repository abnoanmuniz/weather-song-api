﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using WeatherSong.Models.Entities;

namespace WeatherSong.Services.Email
{
    public class EmailService : IEmailService
    {
        public Task RemindPassword(User user, string password)
        {
            MailMessage message = fillMessage(user);


            Dictionary<string, string> htmlKeys = new Dictionary<string, string>
            {
                { "USER_NAME", user.Name},
                { "BODY_CONTENT", "Here is your atual password :" + $"{password}" },
            };

            foreach (var param in htmlKeys)
            {
                if (message.Body.IndexOf(param.Key) != -1)
                {
                    message.Body = message.Body.Replace(param.Key, param.Value);
                }
            }

            SmtpClient smtpClient = FillSmtp();

            try
            {
                smtpClient.Send(message);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return null;
        }


        #region private methods
        private static SmtpClient FillSmtp()
        {
            SmtpClient smtpClient = new SmtpClient
            {
                Port = 587,
                Host = "smtp.gmail.com", //for gmail host  
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("weathersongapi@gmail.com", "123456789@SONG"),
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            return smtpClient;
        }
        private static MailMessage fillMessage(User user)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress("weathersong@ws.net");
            message.To.Add(new MailAddress(user.Email));
            message.Subject = "Weather Song - Password Reminder";
            message.IsBodyHtml = true;
            message.Body = "<div id='emailBody'><p><br/><br/><h2>Dear USER_NAME</h2></p><p><br/></p><p>Password reminder.<br/><br/></p><p>  BODY_CONTENT </p> <p><br/><br/></p> <p>Thank You,<br/></p><p>Weather Song Admin</p> <br/> </div><br/>";
            return message;
        }
        #endregion
    }
}
