﻿using AutoMapper;
using WeatherSong.Models.DTOs;
using WeatherSong.Models.Entities;
using WeatherSong.Utils;

namespace WeatherSong.Services.Mapper
{
    public class PersonalNotes : Profile
    {
        public PersonalNotes()
        {

            CreateMap<PersonalNote, PersonalNoteDTO>()
               .ForMember(s => s.Note, opt => opt.MapFrom(src => src.Note))
               .ForPath(s => s.User, opt => opt.MapFrom(src => src.User))
               .ForMember(s => s.UserId, opt => opt.MapFrom(src => src.UserId))
               .ForMember(s => s.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate));

            CreateMap<PersonalNoteDTO, PersonalNote>()
              .ForMember(s => s.Note, opt => opt.MapFrom(src => Md5Util.ConvertStringToHash(src.Note)))
              .ForPath(s => s.User, opt => opt.MapFrom(src => src.User))
              .ForMember(s => s.UserId, opt => opt.MapFrom(src => src.UserId))
              .ForMember(s => s.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate));

        }
    }
}
