﻿using AutoMapper;
using WeatherSong.Models.DTOs;
using WeatherSong.Models.Entities;
using WeatherSong.Utils;

namespace WeatherSong.Services.Mapper
{
    public class UserMapper : Profile
    {
        public UserMapper()
        {                    

            CreateMap<User, UserDTO>()
               .ForMember(s => s.Email, opt => opt.MapFrom(src => src.Email))
               .ForMember(s => s.Hometown, opt => opt.MapFrom(src => src.Hometown))
               .ForMember(s => s.Name, opt => opt.MapFrom(src => src.Name))
               .ForMember(s => s.Password, opt => opt.MapFrom(src => src.Password))
               .ForPath(s => s.PersonalNotes, opt => opt.MapFrom(src => src.PersonalNotes));

            CreateMap<UserDTO, User>()
              .ForMember(s => s.Email, opt => opt.MapFrom(src => src.Email))
              .ForMember(s => s.Hometown, opt => opt.MapFrom(src => src.Hometown))
              .ForMember(s => s.Name, opt => opt.MapFrom(src => src.Name))
              .ForPath(s => s.PersonalNotes, opt => opt.MapFrom(src => src.PersonalNotes))
              .ForMember(s => s.Password, opt => opt.MapFrom(src => Md5Util.ConvertStringToHash(src.Password)));  
        }
    }
}
