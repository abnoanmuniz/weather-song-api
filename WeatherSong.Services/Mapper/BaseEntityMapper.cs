﻿using AutoMapper;
using WeatherSong.Models.DTOs;
using WeatherSong.Models.Entities;

namespace WeatherSong.Services.Mapper
{
    public class BaseEntityMapper : Profile
    {
        public BaseEntityMapper()
        {
            CreateMap<BaseEntity, BaseEntityDTO>()
                    .ForMember(s => s.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(s => s.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                    .ForMember(s => s.ModifiedDate, opt => opt.MapFrom(src => src.ModifiedDate))
                    .ReverseMap();
        }
    }
}
