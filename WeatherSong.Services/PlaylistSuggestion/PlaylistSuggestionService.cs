﻿using Flurl;
using Flurl.Http;
using SpotifyAPI.Web;
using System;
using System.Threading.Tasks;
using WeatherSong.Models.Enum;
using WeatherSong.Models.Response;
using WeatherSong.Utils.Constants;

namespace WeatherSong.Services.PlaylistSuggestion
{
    public class PlaylistSuggestionService : IPlaylistSuggestionService
    {       
        public async Task<BaseResponse<object>> RecomendPlaylist(string hometown, string spotifyToken)
        {
            string categoryName = GetCategoryNameByUserHometown(hometown);

            var spotify = new SpotifyClient(spotifyToken);
            var category = await spotify.Browse.GetCategory(categoryName.ToLower());

            var playlist = await spotify.Browse.GetCategoryPlaylists(category.Id);

            var response = new BaseResponse<object>
            {
                Data = playlist
            };

            return response;
        }

        #region private Methods
        private string GetCategoryNameByUserHometown(string hometown)
        {
            ResponseWeather weather = "http://api.openweathermap.org/data/2.5/weather?"
                                        .SetQueryParams(new { q = hometown, appid = OpenWeatherApiConstants.token })
                                        .GetAsync()
                                        .ReceiveJson<ResponseWeather>()
                                        .Result;
            string categoryName;
            if (weather.Main.TempCelsius > 30.0)
                categoryName = CategoryName.Party.ToString();
            else if (weather.Main.TempCelsius <= 30.0 && weather.Main.TempCelsius >= 15.0)
                categoryName = CategoryName.Pop.ToString();
            else if (weather.Main.TempCelsius <= 14.0 && weather.Main.TempCelsius >= 10.0)
                categoryName = CategoryName.Rock.ToString();
            else
                categoryName = CategoryName.Classical.ToString();
            return categoryName;
        }
        #endregion
    }
}
