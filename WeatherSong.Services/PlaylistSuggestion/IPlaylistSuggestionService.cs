﻿using System;
using System.Threading.Tasks;
using WeatherSong.Models.Response;

namespace WeatherSong.Services.PlaylistSuggestion
{
    public interface IPlaylistSuggestionService
    {
        Task<BaseResponse<Object>> RecomendPlaylist(string hometown, string spotifyToken);
    }
}
