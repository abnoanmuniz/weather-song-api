﻿using System.Threading.Tasks;
using WeatherSong.Models.DTOs;
using WeatherSong.Models.Entities;
using WeatherSong.Models.Response;

namespace WeatherSong.Services.PersonalNotes
{
    public interface IPersonalNoteService
    {
        Task<BaseResponse<object>> GetById(int personalNoteId);
        Task<BaseResponse<PersonalNote>> GetByUserId(int userId);
        Task<BaseResponse<object>> Add(PersonalNoteDTO personalNote, int userId);
        Task<BaseResponse<object>> Update(PersonalNoteDTO personalNote);
        Task<BaseResponse<object>> Delete(int personalNoteId);
    }
}
