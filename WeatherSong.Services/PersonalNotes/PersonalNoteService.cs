﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherSong.Models.DTOs;
using WeatherSong.Models.Entities;
using WeatherSong.Models.Response;
using WeatherSong.Repository.SQL.PersonalNotes;
using WeatherSong.Repository.SQL.Users;

namespace WeatherSong.Services.PersonalNotes
{
    public class PersonalNoteService : IPersonalNoteService
    {
        private readonly IMapper mapper;
        private readonly IPersonalNoteRepository personalNoteRepository;
        private readonly IUserRepository userRepository;
        public PersonalNoteService(IMapper mapper, IPersonalNoteRepository personalNoteRepository, IUserRepository userRepository)
        {
            this.mapper = mapper;
            this.personalNoteRepository = personalNoteRepository;
            this.userRepository = userRepository;
        }
        public async Task<BaseResponse<object>> Add(PersonalNoteDTO personalNote, int userId)
        {
            var user = await userRepository.GetById(userId);
            if (personalNote is null)
            {
                throw new ArgumentNullException(nameof(personalNote));
            }
            user.PersonalNotes.Add(mapper.Map<PersonalNote>(personalNote));
            var response = new BaseResponse<object>
            {
                Data = user
            };
            return response;
        }

        public async Task<BaseResponse<object>> Delete(int personalNoteId)
        {
            await personalNoteRepository.GetByIdAsNoTracking(personalNoteId);
            await personalNoteRepository.Delete(personalNoteId);

            var response = new BaseResponse<object> { };
            return response;
        }

        public async Task<BaseResponse<object>> GetById(int personalNoteId)
        {
            var model = await personalNoteRepository.GetById(personalNoteId);
            var response = new BaseResponse<object>
            {
                Data = model
            };
            return response;
        }

        public async Task<BaseResponse<PersonalNote>> GetByUserId(int userId)
        {
            IList<PersonalNote> entities = (IList<PersonalNote>)await personalNoteRepository.FindAllAsync(s => s.UserId == userId);

            var response = new BaseResponse<PersonalNote>
            {
                Entities = entities
            };
            return response;
        }

        public async Task<BaseResponse<object>> Update(PersonalNoteDTO personalNote)
        {
            var entity = await personalNoteRepository.GetById(personalNote.Id);
            var entityToUpdate = mapper.Map(personalNote, entity);
            await personalNoteRepository.Update(entityToUpdate);

            var response = new BaseResponse<object>
            {
                Data = mapper.Map<UserDTO>(entity)
            };
            return response;
        }
    }
}
