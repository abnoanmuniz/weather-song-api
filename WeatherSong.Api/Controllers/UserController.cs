﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WeatherSong.Api.Authorization;
using WeatherSong.Models;
using WeatherSong.Models.DTOs;
using WeatherSong.Models.Response;
using WeatherSong.Services.Users;


namespace WeatherSong.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        /// <summary>
        /// Authenticate a user in the application.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("authenticate")]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            var response = userService.Authenticate(model);

            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(response);
        }

        /// <summary>
        /// Get's an user by id.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{userId}")]
        public async Task<ActionResult<BaseResponse<object>>> Get(int userId)
        {
            BaseResponse<object> response;
            try
            {
                response = await userService.GetById(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(response);
        }

        /// <summary>
        /// Create a user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<ActionResult<BaseResponse<object>>> Create(UserDTO user)
        {
            BaseResponse<object> response;
            try
            {
                response = await userService.Create(user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(response);
        }

        /// <summary>
        /// Update an user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        public async Task<ActionResult<BaseResponse<object>>> Update(UserDTO user)
        {
            BaseResponse<object> response;
            try
            {              
                response = await userService.Update(user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(response);
        }

        /// <summary>
        /// Deletes an user by id.
        /// </summary>
        /// <param name="userId"></param>
        [Authorize]
        [HttpDelete("Delete/{userId}")]
        public async Task<ActionResult<BaseResponse<object>>> Delete(int userId)
        {
            BaseResponse<object> response;
            try
            {                
                response = await userService.Delete(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(response);
        }

        /// <summary>
        /// Mechanism to reset an user's passwords.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newPassword"></param>       
        /// <returns></returns>       
        [HttpGet("ResetPassword/{userId}")]
        public async Task<ActionResult<BaseResponse<object>>> ResetPassword(int userId, string newPassword)
        {
            BaseResponse<object> response;
            try
            {
                response = await userService.ResetPassword(userId, newPassword);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(response);
        }

        /// <summary>
        /// Mechanism to reminder an user passwprd.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>      
        [HttpGet("RemindPassword/{userId}")]
        public async Task<ActionResult<BaseResponse<object>>> RemindPassword(int userId)
        {
            BaseResponse<object> response;
            try
            {              
                response = await userService.RemindPassword(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(response);
        }
    }
}
