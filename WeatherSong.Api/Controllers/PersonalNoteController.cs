﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WeatherSong.Api.Authorization;
using WeatherSong.Models.DTOs;
using WeatherSong.Models.Entities;
using WeatherSong.Models.Response;
using WeatherSong.Services.PersonalNotes;

namespace WeatherSong.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonalNoteController : ControllerBase
    {
        private readonly IPersonalNoteService personalNoteService;
        public PersonalNoteController(IPersonalNoteService personalNoteService)
        {
            this.personalNoteService = personalNoteService;
        }


        /// <summary>
        /// Get's an personalNote by id.
        /// </summary>
        /// <param name="personalNoteId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{personalNoteId}")]
        public async Task<ActionResult<BaseResponse<object>>> Get(int personalNoteId)
        {
            BaseResponse<object> response;
            try
            {
                response = await personalNoteService.GetById(personalNoteId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(response);
        }

        /// <summary>
        /// Gets an list of personal note by user id.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetByUserId/{userId}")]
        public async Task<ActionResult<BaseResponse<PersonalNote>>> GetByUserId(int userId)
        {
            BaseResponse<PersonalNote> response;
            try
            {
                response = await personalNoteService.GetByUserId(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(response);
        }

        /// <summary>
        /// Add a personal note to an user.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="personalNote"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("Add/{userId}")]
        public async Task<ActionResult<BaseResponse<object>>> Add(int userId, [FromBody] PersonalNoteDTO personalNote)
        {
            BaseResponse<object> response;
            try
            {
                //TODO - Authorizarion
                response = await personalNoteService.Add(personalNote, userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(response);
        }

        /// <summary>
        /// Update an personal note.
        /// </summary>
        /// <param name="personalNote"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        public async Task<ActionResult<BaseResponse<object>>> Update(PersonalNoteDTO personalNote)
        {
            BaseResponse<object> response;
            try
            {
                //TODO - Authorizarion
                response = await personalNoteService.Update(personalNote);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(response);
        }

        /// <summary>
        /// Deletes an personal note by id.
        /// </summary>
        /// <param name="personalNoteId"></param>
        [Authorize]
        [HttpDelete("Delete/{personalNoteId}")]
        public async Task<ActionResult<BaseResponse<object>>> Delete(int personalNoteId)
        {
            BaseResponse<object> response;
            try
            {
                //TODO - Authorizarion
                response = await personalNoteService.Delete(personalNoteId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(response);
        }
    }
}
