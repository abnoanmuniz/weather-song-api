﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WeatherSong.Api.Authorization;
using WeatherSong.Models.Response;
using WeatherSong.Services.PlaylistSuggestion;
using WeatherSong.Utils.Constants;

namespace WeatherSong.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlaylistSuggestionController : ControllerBase
    {
        private readonly IPlaylistSuggestionService playlistSuggestion;
        public PlaylistSuggestionController(IPlaylistSuggestionService playlistSuggestion)
        {
            this.playlistSuggestion = playlistSuggestion;
        }
        /// <summary>
        /// Recommends a playlist for the user based on his location.
        /// In case our Spotify token is expired, please, generate another in the link below and send it as a parameter.
        /// https://developer.spotify.com/console/get-browse-category/?category_id=pop&country=&locale=
        /// </summary>
        /// <param name="hometown"></param>     
        /// <returns></returns>
        [Authorize]
        [HttpGet("RecomendPlaylist/{hometown}/spotifyToken/{spotifyToken}")]
        public async Task<ActionResult<BaseResponse<object>>> RecomendPlaylist(string hometown, string spotifyToken = SpotifyApiConstants.token)
        {
            BaseResponse<object> response;
            try
            {
                response =  await playlistSuggestion.RecomendPlaylist(hometown, spotifyToken);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(response);
        }
    }
}
