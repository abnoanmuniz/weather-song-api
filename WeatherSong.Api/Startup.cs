using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using WeatherSong.Api.Authorization;
using WeatherSong.Models.Response;
using WeatherSong.Repository;
using WeatherSong.Repository.SQL.PersonalNotes;
using WeatherSong.Repository.SQL.Users;
using WeatherSong.Services.Email;
using WeatherSong.Services.PersonalNotes;
using WeatherSong.Services.PlaylistSuggestion;
using WeatherSong.Services.Users;
using WeatherSong.Utils;
using WeatherSong.Utils.Exceptions;

namespace WeatherSong.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddResponseCompression();
            services.AddControllers();
            // Add CORS policy
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                builder =>
                {
                    builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().WithExposedHeaders("Content-Disposition");
                });
            });
            services.AddOptions();
            services.AddMvcCore();

            var appSettings = new WsSettings
            {
                ConnectionString = @"Server=weathersong.czgc7qbpg1mt.us-east-2.rds.amazonaws.com,1433;Database=WeatherSong;USER ID=admin;PASSWORD=123456789;Connection Lifetime=0;Enlist=true;Max Pool Size=10;Min Pool Size=0;Pooling=true"
            };

            services.Configure<WsSettings>(Configuration.GetSection("appsettings"));
            services.AddSingleton(appSettings);
            var optionsBuilder = new DbContextOptionsBuilder<WsDbContext>();

            optionsBuilder.UseSqlServer(appSettings.ConnectionString, opt => opt.CommandTimeout(360));

            optionsBuilder.UseLazyLoadingProxies();
            optionsBuilder.EnableSensitiveDataLogging();

            services.AddTransient(provider =>
            {
                return new WsDbContext(optionsBuilder.Options);
            }
            );

            #region Services and Repos

            services.AddTransient<IPlaylistSuggestionService, PlaylistSuggestionService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IPersonalNoteService, PersonalNoteService>();
            services.AddTransient<IEmailService, EmailService>();

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IPersonalNoteRepository, PersonalNoteRepository>();
            #endregion
            services.AddMemoryCache();

            #region AutoMapper Configuration
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddMaps(new[] {
                    "WeatherSong.Api",
                    "WeatherSong.Services"
                            });
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            #endregion

        
            services.AddControllersWithViews()
                    .AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            #region Swagger Configuration
            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "WeatherSong Api",
                        Version = "v1.0",
                        Description = "ASP.Net Core 3.1 Application API",
                    }
                );
                options.CustomSchemaIds(x => x.FullName);
                

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);

                options.AddSecurityDefinition(
                    "Bearer",
                    new OpenApiSecurityScheme
                    {
                        In = ParameterLocation.Header,
                        Description = "Description: On the value field, copy 'Bearer {YOUR_TOKEN}'",
                        Name = "Authorization",
                        Type = SecuritySchemeType.ApiKey
                    });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                });
            });
            #endregion

            #region Authentication

            //services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            //    .AddCookie(options =>
            //    {

            //    });

            //services.AddAuthentication(o => {
            //    o.DefaultScheme = SchemesNamesConst.TokenAuthenticationDefaultScheme;
            //})
            //.AddScheme<TokenAuthenticationOptions, TokenAuthenticationHandler>(SchemesNamesConst.TokenAuthenticationDefaultScheme, o => { });

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();
            app.UseCors("CorsPolicy");

      

            if (env.EnvironmentName.Equals("Development"))
            {
                app.UseDeveloperExceptionPage();
            }
            //Manage all throw exceptions.
            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    var errorFeature = context.Features.Get<IExceptionHandlerFeature>();
                    var exception = errorFeature.Error;
                    var responseDetails = new BaseResponse<Object>();
                    switch (exception)
                    {
                        case EntityNotFoundException entityNotFoundException:
                        case ArgumentNullException argumentNullException:
                        case ArgumentException argumentException:
                            responseDetails.CreateResponse(exception);
                            responseDetails.httpStatusCode = System.Net.HttpStatusCode.NotFound;
                            break;
                        case UnauthorizedAccessException unauthorizedAccessException:
                            responseDetails.httpStatusCode = System.Net.HttpStatusCode.Unauthorized;
                            break;
                        default:
                            responseDetails.httpStatusCode = System.Net.HttpStatusCode.BadRequest;
                            break;
                    }
                    responseDetails.CreateResponse(exception);
                    string response = JsonConvert.SerializeObject(responseDetails, Formatting.None, new JsonSerializerSettings()
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    context.Response.StatusCode = Convert.ToInt32(responseDetails.httpStatusCode);
                    context.Response.ContentType = "application/problem+json";
                    await context.Response.WriteAsync(response);
                });
            });

            app.UseSwagger(options =>
            {
            });

    
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseResponseCompression();

            // custom jwt auth middleware
            app.UseMiddleware<JwtMiddleware>();
         

            app.UseSwaggerUI(opt =>
            {
                opt.DocumentTitle = "WeatherSong Api";
                opt.SwaggerEndpoint("v1/swagger.json", "Api");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });
        }
    }
}
