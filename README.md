# Weather Song API

This is an application to returns a playlist's for the user based on him location Weather.

## Getting Started

For utilize the API, please follow these steps below:

### First

POST - /api/User/Create

With the example:

Obs.: The name and password set here, is going to be utilized to login into the application.

Request:
```
{
  "name": "Abnoan",
  "email": "abnoanmuniz@gmail.com",
  "hometown": "Recife",
  "password": "123456",
  "personalNotes": [
    {
      "note": "Sample"
    }
  ]
}
```
Response:
```
{
  "httpStatusCode": 0,
  "message": null,
  "data": {
    "name": "Abnoan",
    "email": "abnoanmuniz@gmail.com",
    "hometown": "Recife",
    "password": "e10adc3949ba59abbe56e057f20f883e",
    "personalNotes": [
      {
        "userId": 6,
        "note": "c5dd1b2697720fe692c529688d3f4f8d",
        "id": 6,
        "createdDate": "2020-08-06T20:47:56.1618414-03:00",
        "modifiedDate": null
      }
    ],
    "id": 6,
    "createdDate": "2020-08-06T20:47:56.2147794-03:00",
    "modifiedDate": null
  },
  "exception": {
    "innerExceptions": null,
    "stackTrace": null
  }
}
```

### Second

POST - /api/User/authenticate

Request:
```
{
  "username": "Abnoan",
  "password": "123456"
}
```

Response:
```
{
  "id": 6,
  "email": "abnoanmuniz@gmail.com",
  "name": "Abnoan",
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYiLCJuYmYiOjE1OTY3NTc3NzUsImV4cCI6MTU5NzM2MjU3NSwiaWF0IjoxNTk2NzU3Nzc1fQ.8HMkwJFc_DX0m21iNksvZj9t0hmR7LVFr_tl_zgW3r4"
}
```

### Third

With the token before returned, now, you can access any endpoint of this API, you only need to pass it on the authorization field of swagger or a postman.

## Endpoints

## Playlist Suggestion
GET - /api/PlaylistSuggestion/RecomendPlaylist/{hometown}/spotifyToken/{spotifyToken}

Recommends a playlist for the user based on his location.
In case our Spotify token is expired, please, generate another in the link below and send it as a parameter.
https://developer.spotify.com/console/get-browse-category/?category_id=pop&country=&locale=

## User

POST - /api/User/authenticate - Authenticate a user in the application.

GET - /api/User/{userId} - Get's an user by id.

POST - /api/User/Create - Create a user.

PUT - /api/User - Update an user.

DELETE - /api/User/Delete/{userId} - Deletes an user by id.

GET - /api/User/ResetPassword/{userId} - Mechanism to reset an user's passwords.

GET - /api/User/RemindPassword/{userId} - Mechanism to reminder an user passwords.

## Personal note

GET - /api/PersonalNote/{personalNoteId} - /api/PersonalNote/{personalNoteId}

GET - /api/PersonalNote/GetByUserId/{userId} - Gets an list of personal note by user id.

POST - /api/PersonalNote/Add/{userId} - Add a personal note to an user.

PUT - /api/PersonalNote - Update an personal note.

DELETE - /api/PersonalNote/Delete/{personalNoteId} - Deletes an personal note by id.

## Authors

* **Abnoan Muniz** - [Linkedin](https://www.linkedin.com/in/abnoanmuniz)
